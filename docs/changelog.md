# Changelog

## 2022 October (22.10)

- add generic attribute on `serviceOffering` for Gaia-X Label criteria.
- update the `Terms & Conditions` and create a dedicated verifiable credential for it.
- add criteria for `Catalogue` service synchronisation.
- improved criteria for `DataExchangeComponent` service negotiation.
- add criteria for `DataResource` containing PII.
- update `InstantiatedVirtualService.hostedOn` criteria to include network location.
- replace the rules of the Gaia-X Compliance service validating the `registrationNumber` against 3rd party APIs by a `registrationNumberIssuer` Trust Anchor.
- nominate the Gaia-X Association as valid `registrationNumberIssuer` (pilot phase only)
- add text to describe how to extends Gaia-X Compliance for domain specific needs
- add reference to the online Gaia-X Compliance APIs
- add diagram mapping the Gaia-X entities of Trust Anchors/Trusted Data Sources/Gaia-X Registry and Issuer/Holder/Verified/Verifiable Data Registry
- add text to validate interaction with a Natural Person (experimental)
- add text introducing Interoperability, Portability, Switchability and Intellectual Property Protection (experimental)
- replace Example chapter by smaller inline example snippets
- content refactoring to reduce the number of chapters.
- update document license to CC BY-4.0-NC-ND

## 2022 June release-candidate (22.06-rc)

**General update**

- replaced ISO 3166-1 country code by [ISO 3166-2](https://en.wikipedia.org/wiki/ISO_3166-2) identifying the principal subdivisions (e.g., provinces or states) inside a country.
- adopted a global versioning system for both the document and the attributes: `YY.MM[.PATCH]`. (Removed `version` per attribute)

**About `LegalParticipant`**

- refactored the mandatory `registrationNumber` attribute for `LegalParticipant`:
  - included several possible formats: `local`, `EUID`, `EORI`, `vatID`, `leiCode`.
  - added `registrationNumber` validation constraints using 3rd party external [Trusted Data Sources](https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors).
- added mandatory signature of mandatory Gaia-X Terms & Conditions.

**About `ServiceOffering`**

- added optional `dataProtectionRegime[]` attribute for a generic approach across various existing data protection regimes. The list will be completed over time.
- added mandatory `dataExport[]` attribute for information on the process to request and export personal and non-personal data out of Service Offering.

**About `Instantiated Virtual Resource`**

- renamed `endpoint` by the generic network layer agnostic term `serviceAccessPoint`

## 2022 April release (22.04)

- First release of the Trust Framework document.
- contains basic `Participant`, `ServiceOffering` and `Resource` modeling.
