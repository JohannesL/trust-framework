# Resource & Subclasses

More detail about the role of a resource in the [Architecture](https://docs.gaia-x.eu/technical-committee/architecture-document/22.09/conceptual_model/#resources) document.

## Resource

A resource that may be aggregated in a Service Offering or exist independently of it.

| Attribute               | Card. | Trust Anchor | Comment                                         |
|-------------------------|-------|--------------|-------------------------------------------------|
| `aggregationOf[]`       | 0..*  | State        | `resources` related to the resource and that can exist independently of it. |
| `name`                  | 0..1     | State        | A human readable name of the data resource |
| `description`           | 0..1     | State        | A free text description of the data resource |

## Physical Resource

A Physical Resource inherits from a Resource.  
A Physical resource is, but not limited to, a datacenter, a bare-metal service, a warehouse, a plant. Those are entities that have a weight and position in physical space.

| Attribute              | Card. | Trust Anchor | Comment                                     |
|------------------------|-------|--------------|---------------------------------------------|
| `maintainedBy[]`       | 1..*  | State        | a list of `participant` maintaining the resource in operational condition and thus having physical access to it. |
| `ownedBy[]`            | 0..*  | State        | a list of `participant` owning the resource. |
| `manufacturedBy[]`     | 0..*  | State        | a list of `participant` manufacturing the resource. |
| `locationAddress[].countryCode` | 1..*  | State   | a list of physical locations in ISO 3166-2 alpha2, alpha-3 or numeric format. |
| `location[].gps`       | 0..*  | State        | a list of physical GPS in [ISO 6709:2008/Cor 1:2009](https://en.wikipedia.org/wiki/ISO_6709) format. |

## Virtual Resource

A Virtual Resource inherits from a Resource.  
A Virtual resource is a resource describing recorded information such as, and not limited to, a dataset, a software, a configuration file, an AI model. Special subclasses of Virtual Resource are `SoftwareResource` and `DataResource`.

```mermaid
classDiagram

VirtualResource <|-- SoftwareResource
VirtualResource <|-- DataResource 

```

| Attribute            | Card. | Trust Anchor | Comment                                   |
|----------------------|-------|--------------|-------------------------------------------|
| `copyrightOwnedBy[]` | 1..*  | State        | A list of copyright owners either as a free form string or `participant` URIs from which Self-Descriptions can be retrieved. A copyright owner is a person or organization that has the right to exploit the resource. Copyright owner does not necessarily refer to the author of the resource, who is a natural person and may differ from copyright owner. |
| `license[]`          | 1..*  | State        | A list of [SPDX](https://github.com/spdx/license-list-data/tree/master/jsonld) identifiers or URL to document|
| `policy[]`           | 1..*  | State        | a list of `policy` expressed using a DSL (e.g., Rego or ODRL) (access control, throttling, usage, retention, ...) |

The `license` refers to the license of the virtual resource - data or software, not the license of potential instance of that virtual resource.

If there is no specified usage policy constraints on the `VirtualResource`, the  `policy` should express a simple `default: allow` intent.

## Data Resource

A `data resource` is a subclass of `virtual resource` exposed through a service instance.  
It's also known as a `Data Product` as described in the Gaia-X Data Exchange specification document.

A `data resource` is extending the [DCAT-3 Dataset class](https://www.w3.org/TR/vocab-dcat-3/#Class:Dataset) and primarily refers to an analytical dataset exposed via one or more `InstantiatedVirtualResource` service access points.

The data resource consists of the characterisation of the actual data as a description of the "contractual" part. At minimum, this self-description needs to contain all information so that a consumer can initiate a `contract negotiation`. All other attributes that are used to describe the data are optional. However, the provider has an interest to precisely describe the data so that it can be found and consumed. If the data resource is published in a catalogue, the data provider might precisely describe the data resource so that it can be found and consumed by data consumers. 

| Attribute            | Card. | Trust Anchor | Comment                                         |
|----------------------|-------|--------------|-------------------------------------------------|
| `producedBy`     | 1     | State        | a resolvable link to the participant self-description legally enabling the data usage |
| `exposedThrough[]`   | 1..*  |`producedBy`| A resolvable link to the data exchange component that exposes the data resource. |
| `obsoleteDateTime`   | 0..1  |`producedBy`| date time in ISO 8601 format after which data is obsolete. |
| `expirationDateTime` | 0..1  |`producedBy`| date time in ISO 8601 format after which data is expired and shall be deleted. |
| `containsPII` | 1 |`producedBy`| boolean determined by Participant owning the Data Resource |
<!-- | `dataController`     | 0     | `producedBy`        | data controller Participant as defined in GDPR. |
| `consent[]`          | 0..*  |`dataController`| list of consents from the data subjects as Natural Person when the dataset contains PII. | -->


_NOTE: Data Resources will have to align with the adequate protection of (personal) data. Data Resources are most likely applicable in the context of Data Spaces respectively Data Sharing Ecosystems (following, both referred to as "Data Space"). Data Spaces require substantial evaluation in regards to different responsibilities and subsequent definition of requirements. As Data Spaces are temporarily excluded from the Policy Rules and Labelling Criteria until such fundamental aspects are resolvers, the Trust Framework will also not extend the attributes unless necessary. 
Saying: Especially when the protection of PII and IPR will be concerned, i.e., the validation of legitimate collection and sharing of data, the Trust Framework may limit its attributes to the identification if such data will be concerned as well as if legitimate grounds are identified. 
More sophisticated approaches, such as tracing legitimate grounds alongside the processing chain, technical enforcement of processing limitations by policies, are subject to future iterations._

**Consistency rules**

- the keypair used to sign the Data Resource claims must be traceable to the `producedBy` participant of the Data Resource.
- if `containsPII`is `true`, attributes related to the Legitimate Processing of Information related to PII become mandatory.
<!-- - If the data are about data subjects as one or more Natural Persons, or sensitive data as defined in GDPR [article 9](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2051-1-1), than `dataController` and `consent` are mandatory.
  To avoid [data re-identification](https://ec.europa.eu/eurostat/cros/content/re-identification_en), this rule applies independently if the data is raw, pseudo-anonymized or anonymized. (Note: This is on purpose beyond GDPR requirements.)
- if `dataController` is specified, the keypair used to sign at least the Data Resource `consent` claims must be traceable to the `dataController`. -->

### Legitimate Processing of Information Related to PII

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `legalBasis`           | 1     |`producedBy` or `ownedBy`| One of the reasons as detailed in the identified Personal Data Protection Regimes, such as [GDPR2018]. Potential Legal Bases can be [article 6](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e1888-1-1), [article 7](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2001-1-1) or [article 9](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2051-1-1). It shall be expressed as a string matching `6.1.[a-f]`, `6.1.4`, `7` or `9.2.[a-j]`.<br> (_Note: this list is not final, as GDPR and Member State Law may provide for additional legal basis. Those will be implemented as options in future iterations._)|
| `dataProtectionContact` | 1    |`producedBy`| means to address distinct personal data protection related questions related to the Data Source, either an URL pointing to a contact form or an emailaddress.|

Where Data Consumers conclude with their individual risk assessment that more information is needed, i.e., in cases where the generic information on the applied legal bases may not suffice but more granular information is considered necessary, each Data Source shall provide for a dedicated contact point.  
The contact point may be a functional email address.

**_NOTE: The Trust Framework admits that any (semi-)automated propagation and enforcement of processing policies, i.e., determination of legitimate processing period, data types, transfers, aggregation, purposes, commercial or non-commercial use will require a sophisticated syntax and inter-related logic of attributes. Against the background of the complexity, the development will be performed iteratively, beginning with the next release._**

<!-- | `consentDuration`      | 1     | State        | duration as a number. See [man sleep(1)](https://man7.org/linux/man-pages/man1/sleep.1.html) for the format. |-->
<!-- | `consentProof`         | 0     | State        | Proof of the data subject consent, captured as described in the GDPR [article 7](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2001-1-1). |
| `purpose[]`            | 0..*  | State        | Purposes of the processing. It is recommended to use well know controlled vocabulary such as the [Data Privacy Vocabulary:Purposes](https://w3c.github.io/dpv/dpv/#vocab-purpose) | 

**Consistency Rules**

- If `legalBasis` is in [`6.1.a`, `9.2.a`], then `consentProof` is mandatory.
- If `legalBasis` is not in [`6.1.a`, `9.2.a`], then `purpose` is mandatory.

The `consentProof` object shall include at least one of the following attributes:
- a certificate testifying from an interaction with the initial Natural Participant using a [RFC7517 `x5c`](https://www.rfc-editor.org/rfc/rfc7517#section-4.7) (X.509 Certificate Chain) attribute or [RFC7517 `x5u`](https://www.rfc-editor.org/rfc/rfc7517#section-4.6) (X.509 URL) attribute.  
  The `x5u` parameter should be resolvable to a `X509` `.crt`, `.pem`, `.der` or `.p7b` file which contains a valid Gaia-X Trust Anchor eligible for the signed claims of Natural Person verification.
- a URL `record` to a proof of the data subject consent, captured as described in the GDPR [article 7](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2001-1-1)

The `consentProof` endpoint might be protected with an access control mechanism. The access control logic should be computed from the self-description of the Participants requesting access to the information.

<details>
  <summary>Example of <code>consent</code></summary>

```json
{
  "consent": [
    {
      "consentDuration": "90d",
      "legalBasis": "6.1.b",
      "purpose": ["Account Management", "Identity Verification"]
    }
  ]
}
```
</details>
--> 
<!--
| 1.0     | `categoryOfData`       | 1     | State        | Category of data as defined in the Data Governance Act article 3 (1) and Article 3 (2). |
To be moved to a data subject consent
-->

<!--
> o       purpose,
> o       time interval,
> o       geographic scope
> o       pass through rights degrees (from 1st party through 3rd party)
> o       eventually 'delete' preference, after use
> o       eventually 'compensation' vs' non compensation requirement

-->
