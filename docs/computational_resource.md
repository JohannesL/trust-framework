# Computational Resource

A `Computational Resource` is a special kind of [Resource](#resource) with physical or non-physical computational capabilities, which interacts via [Interconnection](#interconnections) with other Resources.

Computational resources are either physical or virtual as defined below. Other kind of computational resource are prohibited.

```mermaid
classDiagram

class Resource{
    <<abstract>>
}

class ComputationalResource {
    <<abstract>>
}


Resource <|-- PhysicalResource  
Resource <|-- ComputationalResource
Resource <|-- VirtualResource 

VirtualResource "1" <-- InstantiatedVirtualResource : instanceOf

InstantiatedVirtualResource --> "1" Resource : hostedOn
PhysicalResource <|-- PhysicalComputationalResource

ComputationalResource <|-- PhysicalComputationalResource
ComputationalResource <|-- VirtualComputationalResource

InstantiatedVirtualResource <|-- VirtualComputationalResource
```


- A Physical Computational Resource is a Computational Resource, whose computational capabilities are provided by hardware, such as servers. Physical Computational Resource  are sub-classes of Physical Resources.
- A Virtual Computational Resource is a Computational Resource  whose computational capabilities are provided by software, such as virtual machines or containers. Virtual Computational Resource are sub-classes of Instantiated Virtual Resources.
